import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import { NutrientFact, Product, Recipe } from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

// sorts an object's attributes by the attribute name
function sortObjectAttributes(obj: any) {
	const sortedObject = {};
	Object.keys(obj).sort().forEach(key => {
	  sortedObject[key] = obj[key];
	});
	return sortedObject;
  }
  
  // calculates the cheapest cost each recipe can be made and recipy facts summary
  function calculateCheapestCostRecipeFactSummary(recipe: Recipe) {
	let cheapestCost = 0;
	const nutrientFactsAtCheapestCost = {};
  
	recipe.lineItems.forEach(lineItem => {
	  const ingredientProducts = GetProductsForIngredient(lineItem.ingredient);
  
	  if (!ingredientProducts || ingredientProducts.length === 0) {
		return;
	  }
  
	  const cheapestProduct = ingredientProducts.reduce((prev: any, curr: Product) => {
		const nutrientFacts = (curr.nutrientFacts || []).map(GetNutrientFactInBaseUnits);
		const costPerBaseUnits = (curr.supplierProducts || []).map(GetCostPerBaseUnit);
  
		const cheapestCostPerBaseUnit = Math.min(...costPerBaseUnits);
		if (prev === null || cheapestCostPerBaseUnit < prev.cheapestCostPerBaseUnit) {
		  return {
			cheapestCostPerBaseUnit,
			nutrientFacts
		  };
		} else {
		  return prev;
		}
	  }, null);
  
	  cheapestCost += cheapestProduct.cheapestCostPerBaseUnit * (lineItem.unitOfMeasure?.uomAmount ?? 0);
  
	  cheapestProduct.nutrientFacts.forEach((fact: NutrientFact) => {
		if (!nutrientFactsAtCheapestCost[fact.nutrientName]) {
		  nutrientFactsAtCheapestCost[fact.nutrientName] = fact;
		} else {
		  nutrientFactsAtCheapestCost[fact.nutrientName].quantityAmount.uomAmount += fact.quantityAmount.uomAmount;
		}
	  });
	});
  
	return {
	  cheapestCost,
	  nutrientsAtCheapestCost: sortObjectAttributes(nutrientFactsAtCheapestCost)
	};
  }
  
  // loops through each recipe and calculates the cheapest cost and nutrient facts for each recipe
  for (const recipe of recipeData) {
	recipeSummary[recipe.recipeName] = calculateCheapestCostRecipeFactSummary(recipe);
  }
 

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
